<?php session_start() ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Hello</title>
        <meta name="description" content="">
    </head>
    <body>
        <h1>Welcome</h1>
        <?php
            echo '<p>Hello, ' . $_SESSION['email'] . '</p>';
            echo '<form action="server.php" method="post">';
            echo '<input type="hidden" name="action" value="logout">';
            echo '<button type="submit">Logout</button>';
            echo '</form>';
        ?>
    </body>
</html>