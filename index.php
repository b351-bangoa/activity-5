<?php session_start();?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Activity 5</title>
    </head>
    <body>
    <h1>Login Form</h1>
            <form method="POST" action='server.php'>
                <label for="email">Email: </label>
                <input type="text" name="email" required>
                <label for="password">Password: </label>
                <input type="password" name="password" required>
                <input type="hidden" name="action" value="login">
                <button type="submit">Login</button>
            </form>
        <?php
        if (!empty($_SESSION['email'])) {
            if (isset($_SESSION['email'])) {
            echo '<form type="hidden" method="POST" action="server.php">';
            echo '<input type="hidden" name="action" value="logout">';
            echo '</form>';
            }
            if (isset($_SESSION['login_error_message'])) {
                echo '<p>'.$_SESSION['login_error_message'].'</p>';
                unset($_SESSION['login_error_message']);
            }
        }
        ?>
    </body>
</html>